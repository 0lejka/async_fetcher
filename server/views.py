from aiohttp import web
import asyncio
import random


async def index(request):
    await asyncio.sleep(1 + random.random())
    return web.Response(text='Hello {}'.format(request.match_info['index']))
