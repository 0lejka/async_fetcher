import aiohttp
import asyncio
import aioredis


async def slow_operation():
    redis = await aioredis.create_redis(
        ('localhost', 6379), loop=loop)
    while True:
        param = await redis.rpop('queue1')
        if param:
            async with aiohttp.request('GET', 'http://localhost:8080/python {}/'.format(param)) as resp:
                assert resp.status == 200
                print (await resp.text())
        else:
          asyncio.sleep(1)
    pool.close()
    await pool.wait_closed()


loop = asyncio.get_event_loop()
future = asyncio.Future()
asyncio.ensure_future(asyncio.wait([slow_operation() for i in range(100)]))
try:
    loop.run_forever()
finally:
    loop.close()
