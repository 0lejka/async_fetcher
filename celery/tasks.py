from celery import Celery
import requests
import logging

logger = logging.getLogger('tasks')

app = Celery('tasks', broker='redis://localhost//')

@app.task
def add(x):
    res = requests.get('http://localhost:8080/%s/' % x)
    logger.info(res.content)
